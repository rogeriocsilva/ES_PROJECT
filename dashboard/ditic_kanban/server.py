# -*- coding: utf-8 -*-
#
# By Pedro Vapi @2015
# This module is responsible for web routing. This is the main web server.
#
from time import time
from datetime import date
import os
import rt_summary
import statistics
global emailGlobal
global ticket_id_comment

from bottle import get
from bottle import post
from bottle import template
from bottle import request
from bottle import run
from bottle import redirect
from bottle import route
from bottle import static_file

from ditic_kanban.rt_summary import get_summary_info
from ditic_kanban.config import DITICConfig
from ditic_kanban.auth import UserAuth
from ditic_kanban.tools import user_tickets_details
from ditic_kanban.tools import ticket_actions
from ditic_kanban.tools import user_closed_tickets
from ditic_kanban.tools import search_tickets
from ditic_kanban.tools import get_urgent_tickets
from ditic_kanban.tools import create_ticket
from ditic_kanban.tools import get_ticket_content
from ditic_kanban.rt_api import RTApi
from ditic_kanban.statistics import get_date
from ditic_kanban.statistics import get_statistics
from ditic_kanban.rt_api import modify_ticket


# My first global variable...
user_auth = UserAuth()

# Only used by the URGENT tickets search
my_config = DITICConfig()
system = my_config.get_system()
rt_object = RTApi(system['server'], system['username'], system['password'])

# This part is necessary in order to get access to sound files
# Static dir is in the parent directory
STATIC_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), "../static"))
print STATIC_PATH


def create_default_result():
    # Default header configuration
    result = {
        'title': 'ES -- site'
    }

    # Summary information
    result.update({'summary': get_summary_info()})

    # Mapping email do uer alias
    config = DITICConfig()
    result.update({'alias': config.get_email_to_user()})

    return result


@get('/')
def get_root():
	start_time = time()


	rt_summary.generate_summary_file()
	rt_summary.get_summary_info()
	statistics.stats_update_json_file()

	result = create_default_result()
	# Removed to be a display at the TV
	#if request.query.o == '' or not user_auth.check_id(request.query.o):
	#    result.update({'message': ''})
	#    return template('auth', result)
	#result.update({'username': user_auth.get_email_from_id(request.query.o)})
	result.update({'username_id': request.query.o})
	today = date.today().isoformat()
	result.update({'statistics': get_statistics(get_date(30, today), today)})

	# Is there any URGENT ticket?
	result.update({'urgent': get_urgent_tickets(rt_object)})

	result.update({'time_spent': '%0.2f seconds' % (time() - start_time)})
	

	return template('entrance_summary', result)

@get('/display/<ticket_id>')
def display(ticket_id):
	result = create_default_result()

	if request.query.o == '' or not user_auth.check_id(request.query.o):
		result.update({'message': ''})
		return template('auth', result)

	result.update({'username_id': request.query.o})
	result.update({'email': user_auth.get_email_from_id(request.query.o)})

	aux = rt_object.get_data_from_rest('ticket/'+ticket_id+'/show', '')
	aux2 = get_ticket_content(user_auth.get_rt_object_from_email(user_auth.get_email_from_id(request.query.o)), ticket_id)
	comentarios = '\nDescription:' +':\n' + aux2[0]
	for i in range(1,len(aux2)):
		comentarios += '\nComment ' +(str)(i) +':\n' + aux2[i]
	
	result.update({'id': ("ID: "+ticket_id)})
	result.update({'Queue':aux[3]})
	result.update({'Owner':aux[4]})
	result.update({'Creator':aux[5]})
	result.update({'Subject':aux[6]})
	result.update({'Status':aux[7]})
	result.update({'Priority':aux[8]})
	result.update({'Description': 'description' +':\n' + aux2[0]})
	result.update({'Created':aux[14]})
	result.update({'Started':aux[16]})
	result.update({'Resolved':aux[18]})
	result.update({'TimeWorked':aux[22]})
	result.update({'CF.{IS - Informatica e Sistemas}': aux[24]})
	result.update({'CF.{Servico}':aux[25]})
	result.update({'CF.{DITIC-Interrupted}':aux[27]})
	result.update({'CF.{Archived}':aux[28]})
	result.update({'Comments': comentarios})
	
	return template('description', result)


@get('/comment/<ticket_id>/<email>')
def justificacao(ticket_id,email):
	
	
	result = create_default_result()

	result.update({'username_id': request.query.o})
	result.update({'email': email})

	result.update({'ticket_id': ticket_id})
	return template('comment', result)


@post('/ticket/<ticket_id>/<email>comment')
def justificacao_2(ticket_id,email):

  	result =create_default_result();

	result.update({'email': email})
	result.update({'username_id': request.query.o})

	comment_ticket = {
		'id': ticket_id,
		'Action': "comment",
		'Text': request.forms.get('justificacao'),
	}
	content = ''
	for key in comment_ticket:
		content += '{0}: {1}\n'.format(key,comment_ticket[key])
	query = {
		'content':content
	}
	rt_object.get_data_from_rest('/ticket/'+ticket_id+'/comment', query)

	redirect("/detail/"+email+"?o="+request.query.o)
	



@post('/auth')
def auth():
    result = create_default_result()
    result.update({'username': request.forms.get('username'), 'password': request.forms.get('password')})
    if request.forms.get('username') and request.forms.get('password'):
        try:
            if user_auth.check_password(request.forms.get('username'), request.forms.get('password')):
                redirect('/?o=%s' % user_auth.get_email_id(request.forms.get('username')))
            else:
                result.update({'message': 'Password incorrect'})
                return template('auth', result)
        except ValueError as e:
            result.update({'message': str(e)})
            return template('auth', result)
    else:
        result.update({'message': 'Mandatory fields'})
        return template('auth', result)


@get('/detail/<email>')
def email_detail(email):
	start_time = time()
	global emailGlobal
	emailGlobal=email
	result = create_default_result()
	if request.query.o == '' or not user_auth.check_id(request.query.o):
		result.update({'message': ''})
		return template('auth', result)

	result.update({'username': user_auth.get_email_from_id(request.query.o)})
	result.update({'email': email})
	result.update({'username_id': request.query.o})
	
	aux = user_tickets_details(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		    ),user_auth.get_email_from_id(request.query.o))

	result.update({'auth_email_limit':aux['email_limit']})
	result.update({'auth_number_tickets_per_status': aux["number_tickets_per_status"]})
	
		

	result.update(user_tickets_details(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		), email))

	# Is there any URGENT ticket?
	result.update({'urgent': get_urgent_tickets(rt_object)})

	result.update({'time_spent': '%0.2f seconds' % (time() - start_time)})


	if email == 'dir' or email == 'dir-inbox' or email == 'unknown':
		return template('ticket_list', result)
	else:
		return template('detail', result)


@get('/closed/<email>')
def email_detail(email):
	start_time = time()

	result = create_default_result()
	if request.query.o == '' or not user_auth.check_id(request.query.o):
		result.update({'message': ''})
		return template('auth', result)

	result.update({'username': user_auth.get_email_from_id(request.query.o)})
	result.update({'email': email})
	result.update({'username_id': request.query.o})

	result.update(user_closed_tickets(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		), email))

	# Is there any URGENT ticket?
	result.update({'urgent': get_urgent_tickets(rt_object)})

	result.update({'time_spent': '%0.2f seconds' % (time() - start_time)})

	
	return template('ticket_list', result)



@post('/search')
def search():
	start_time = time()

	result = create_default_result()
	if request.query.o == '' or not user_auth.check_id(request.query.o):
		result.update({'message': ''})
		return template('auth', result)

	if not request.forms.get('search'):
		redirect('/?o=%s' % request.query.o)
	search = request.forms.get('search')

	result.update({'username': user_auth.get_email_from_id(request.query.o)})
	result.update({'email': search})
	result.update({'username_id': request.query.o})

	result.update(search_tickets(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		), search))

	# Is there any URGENT ticket?
	result.update({'urgent': get_urgent_tickets(rt_object)})

	result.update({'time_spent': '%0.2f seconds' % (time() - start_time)})

	return template('search', result)


@get('/ticket/<ticket_id>/action/<action>')
def ticket_action(ticket_id,action):
	global emailGlobal
	ticket_action2(ticket_id,action,emailGlobal)
	rt_summary.generate_summary_file()
	rt_summary.get_summary_info()
	statistics.stats_update_json_file()
	redirect("/detail/"+emailGlobal+"?o="+request.query.o)


def ticket_action2(ticket_id, action,query_email):
	start_time = time()

	print("-----------------_>"+request.query.email)
	result = create_default_result()
	if request.query.o == '' or not user_auth.check_id(request.query.o):
		result.update({'message': ''})
		return template('auth', result)

	# Apply the action to the ticket
	result.update(ticket_actions(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		),
		ticket_id,
		action,
		query_email, user_auth.get_email_from_id(request.query.o)
	))

	

	# Update table for this user
	result.update(user_tickets_details(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		), query_email))

	aux = user_tickets_details(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		    ),user_auth.get_email_from_id(request.query.o))

	result.update({'auth_email_limit':aux['email_limit']})
	result.update({'auth_number_tickets_per_status': aux["number_tickets_per_status"]})


	result.update({'username': user_auth.get_email_from_id(request.query.o)})
	result.update({'email': query_email})
	result.update({'username_id': request.query.o})

	# Is there any URGENT ticket?
	result.update({'urgent': get_urgent_tickets(rt_object)})

	result.update({'time_spent': '%0.2f seconds' % (time() - start_time)})

	#update all

	if query_email == 'dir' or query_email == 'dir-inbox' or query_email == 'unknown':
		return template('ticket_list', result)
	else:
		return template('detail', result)

@post('/ticket/new')
def create():
  result =create_default_result();

  if request.query.o == '' or not user_auth.check_id(request.query.o):
    result.update({'message': ''})
    return template('auth', result)

  if not request.forms.get('subject'):
    redirect('/?o=%s' % request.query.o)
  if not request.forms.get('description'):
    redirect('/?o=%s' % request.query.o)

  create_ticket(user_auth.get_rt_object_from_email(user_auth.get_email_from_id(request.query.o)),request.forms.get('subject'),request.forms.get('description'))
  
  redirect('/?o=%s' % request.query.o)

@get('/resolve/<ticket_id>/<email>')
def justificacao(ticket_id,email):
	
	result = create_default_result()

	result.update({'username_id': request.query.o})
	result.update({'email': email})

	result.update({'ticket_id': ticket_id})

	return template('resolve', result)

@post('/ticket/<ticket_id>/<email>/resolve')
def resolve(ticket_id,email):
	global emailGlobal
	result =create_default_result()

	result.update({'email': email})
	result.update({'username_id': request.query.o})

	comment_ticket = {
		'id': ticket_id,
		'Action': "comment",
		'Text': "Resolved due - "+request.forms.get('justificacao'),
	}
	content = ''
	for key in comment_ticket:
		content += '{0}: {1}\n'.format(key,comment_ticket[key])
	query = {
		'content':content
	}
	rt_object.get_data_from_rest('/ticket/'+ticket_id+'/comment', query)
	ticket_action2(ticket_id,'done',emailGlobal)

	redirect("/detail/"+emailGlobal+"?o="+request.query.o)

@get('/archive/<email>')
def archive(email):
	global emailGlobal

	result = create_default_result()
	if request.query.o == '' or not user_auth.check_id(request.query.o):
		result.update({'message': ''})
		return template('auth', result)

	result.update({'username': user_auth.get_email_from_id(request.query.o)})
	result.update({'email': email})
	result.update({'username_id': request.query.o})

	tickets_query = user_closed_tickets(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		), email)

	for tickets in tickets_query['tickets']:
		for i in tickets_query['tickets'][tickets]:
			modify_ticket(
					user_auth.get_rt_object_from_email(
			    user_auth.get_email_from_id(request.query.o)
			),
					i['id'],
					{
						'cf.{Archived}': 'yes',
					}
				)
	redirect("/detail/"+emailGlobal+"?o="+request.query.o)

@get('/unarchive/<email>/<ticket_id>/')
def unarchive(email,ticket_id):
	global emailGlobal

	result = create_default_result()
	if request.query.o == '' or not user_auth.check_id(request.query.o):
		result.update({'message': ''})
		return template('auth', result)

	result.update({'username': user_auth.get_email_from_id(request.query.o)})
	result.update({'email': email})
	result.update({'username_id': request.query.o})
	print({'username': user_auth.get_email_from_id(request.query.o)})
	print({'email': email})
	print({'username_id': request.query.o})
	print(ticket_id)

	tickets_query = user_closed_tickets(
		user_auth.get_rt_object_from_email(
		    user_auth.get_email_from_id(request.query.o)
		), email)

	for tickets in tickets_query['tickets']:
		for i in tickets_query['tickets'][tickets]:
			print('--------------------------->')
			print(i)
			modify_ticket(
					user_auth.get_rt_object_from_email(
			    user_auth.get_email_from_id(request.query.o)
			),
					ticket_id,
					{
						'owner': 'nobody',
						'cf.{archived}': 'no',
						'cf.{is - informatica e sistemas}': 'dir-inbox',
						'status': 'new',
					}
				)

	redirect("/?o="+request.query.o)


@route("/static/<filepath:path>", name="static")
def static(filepath):
    return static_file(filepath, root=STATIC_PATH)


def start_server():
    run(server='paste', host='0.0.0.0', debug=True)

if __name__ == '__main__':
    start_server()
