<!DOCTYPE html>

<html>
 <style media="screen" type="text/css">
 * * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-weight: 300;
    text-decoration: none;
    color: rgba(0, 0, 0, 0.82);
}

hr {
    border: 0;
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    width: 900px;
}

body {
    background-color: rgb(242, 242, 242);
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    margin: 0;
}

body::-webkit-input-placeholder {
    /* WebKit browsers */
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body:-moz-placeholder {
    /* Mozilla Firefox 4 to 18 */
    font-family: 'Roboto', sans-serif;
    opacity: 1;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body::-moz-placeholder {
    /* Mozilla Firefox 19+ */
    font-family: 'Roboto', sans-serif;
    opacity: 1;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body:-ms-input-placeholder {
    /* Internet Explorer 10+ */
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

.wrapper {
    background-color: rgb(242, 242, 242);
    position: absolute;
    top: 50%;
    left: 0;
    width: 100%;
    height: 400px;
    margin-top: -200px;
    overflow: hidden;
}

.wrapper.form-success .container h1 {
    -webkit-transform: translateY(85px);
    -ms-transform: translateY(85px);
    transform: translateY(85px);
    background-color: rgb(242, 242, 242);
}

.container {
    max-width: 370px;
    margin: 0 auto;
    padding: 80px 0;
    height: 400px;
    text-align: center;
}

.container_t {
    max-width: 900px;
    margin: 0 auto;
    padding: 80px 0;
    height: 400px;
    text-align: center;
}

.container h1 {
    font-size: 40px;
    -webkit-transition-duration: 1s;
    transition-duration: 1s;
    -webkit-transition-timing-function: ease-in-put;
    transition-timing-function: ease-in-put;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}

.form_l input {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    background-color: rgb(255, 255, 255);
    width: 250px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l input:hover {
    background-color: rgba(255, 255, 255, 0.4);
}

.form_l input:focus {
    background-color: white;
    width: 300px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    font-family: 'Roboto';
}

.form_l button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    background-color: rgb(255, 255, 255);
    width: 100px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l button:hover {}

.form_t {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
    text-align: center;
}

.form_t input {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    background-color: rgb(255, 255, 255);
    width: 800px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: left;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_t input:hover {
    background-color: rgba(255, 255, 255, 0.4);
}

.form_t input:focus {
    background-color: white;
    width: 850px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    font-family: 'Roboto';
}

.form_t button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    background-color: rgb(255, 255, 255);
    width: 200px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_t button:hover {}

nav {
    display: inline-block;
    width: 100%;
    height: 60px;
    background-color: #3F51B5;
    padding-left: 40px;
    box-shadow: 2px 3px 10px #888888;
}

nav div {
    margin: 5px;
    float: left;
    height: 100%;
}

nav h1 {
    color: white;
    font-weight: 300;
    font-family: 'Roboto';
    font-size: 20pt;
    margin-top: 7px;
}

.account {
    float: right;
}

.user {
    float: right;
}

.tabela {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
    width: 900px;
    padding: 20px;
}

.tabela_out {
    padding: 20px 0;
    width: 900px;
    margin: 0 auto;
}

#performance {
    padding: 20px 0;
    margin-left: 35.5%;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}

#mean_time_to_resolve {
    padding: 20px 0;
    margin-left: 10%;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}
</style>


% result = "['Date', 'Created', 'Resolved', 'Still open'],\n"
% for day in sorted(statistics):
%   if statistics[day]:
%       result += '''["%s", %s, %s, %s],\n''' % (day,
%       statistics[day]["created_tickets"],
%       statistics[day]['team']['resolved'],
%       statistics[day]['team']['open'])
%   else:
%       result += '["%s", 0, 0, 0],\n' % day
%   end
% end

% graph_script = """
    <script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>

    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
%s
        ]);

        var options = {
          title: 'Número de tickets',
          curveType: 'function',
          legend: { position: 'bottom' },
        };

        var chart = new google.visualization.LineChart(document.getElementById('performance'));

        chart.draw(data, options);
      }
    </script>
""" % result

% result = "['Date', 'Mean Time to Resolve', 'Time worked'],\n"
% for day in sorted(statistics):
%   if statistics[day]:
%       result += '''["%s", %s, %s],\n''' % (day,
%       statistics[day]['team']['mean_time_to_resolve']/60,
%       statistics[day]['team']['time_worked']/60)
%   else:
%       result += '["%s", 0, 0],\n' % day
%   end
% end
% graph_script += """
 <script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>

    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
%s
        ]);

        var options = {
          title: 'Tempo médio de resolução vs Tempo total trabalhado (horas)',
          curveType: 'function',
          legend: { position: 'bottom' },
        };

        var chart = new google.visualization.LineChart(document.getElementById('mean_time_to_resolve'));

        chart.draw(data, options);
      }
    </script>
""" % result

% rebase('skin')
<head>
    <meta charset="UTF-8">
    <title>es — site</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,,200,300,500,700' rel='stylesheet' type='text/css'>



</head>



<body>



    <!-- cada td é uma coluna da tabela -->

    <div class=tabela_out>
        <table class="tabela">
            <tr>
                <td align="left"><a href="/detail/dir?o={{username_id}}"><h1>DIR</h1></a>
                </td>
                 % # DIR
        % sum = 0
        % # we need this code because DIR can have tickets all along several status
        % for status in summary['dir']:
        %   sum += summary['dir'][status]
        % end
        <td align="center" valign="top"><h1>{{sum}}</h1></td>
            </tr>
            <tr>
                <td align="left"><a href="/detail/dir-inbox?o={{username_id}}"><h1>DIR-INBOX</h1></a>
                </td>
                        % # DIR-INBOX
        % sum = 0
        % # we need this code because DIR can have tickets all along several status
        % for status in summary['dir-inbox']:
        %   sum += summary['dir-inbox'][status]
        % end
        <td align="center" valign="top">
            <h1>{{sum}}</h1>
            % urgent = get('urgent', '') 
            % if urgent:
                <h4 style="color:red;">URGENT!</h4>
            %end
        </td>
            </tr>
        </table>
    </div>
    <div class="tabela_out">
        <h1>DITIC Kanban Board</h1>
        <table class="tabela">


            <tr>
                <td align="left"><h2>USER</h2></td>
                <td align="left"><h2>IN</h2></td>
                <td align="left"><h2>ACTIVE</h2></td>
                <td align="left"><h2>STALLED</h2></td>
                <td align="left"><h2>DONE</h2></td>
            </tr>
         
                % totals = { status: 0 for status in ['new', 'open', 'stalled', 'resolved']}
                % for email in sorted(summary):
                %   if email.startswith('dir'):
                %       continue
                %   end
                %   user = email
                %   if  email != 'unknown':
                %       user = alias[email]
                %   end



            <tr>
                <td><a href="/detail/{{email}}?o={{username_id}}">{{user}}</a></td>
                %   for status in ['new', 'open', 'stalled', 'resolved']:
                <td>{{summary[email][status]}}</td>
                %       totals[status] += summary[email][status]
                % end
            </tr>
            %end


            <tr>
    <td><strong>Totais</strong></td>
    % for status in ['new', 'open', 'stalled', 'resolved']:
    <td><strong>{{totals[status]}}</strong></td>
    % end
</tr>

        </table>
    </div>

    <!-- importar merdas, os gráficos e personalizar, porque eu não consgio ver aqui-->

    <table>
        <td>
            <div id="performance" style="width: 400px; height: 400px"></div>
        </td>
        <td>
            <div id="mean_time_to_resolve" style="width: 400px; height: 400px"></div>
        </td>
    </table>

<!--

    <script src="/js/jquery.min.js"></script>
    <script src="/js/skel.min.js"></script>
    <script src="/js/util.js"></script>
    [if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]
    <script src="/js/main.js"></script>
-->



<div class="container_t">
            <h1 align="left">Novo Ticket</h1>
            <form class="form_t" action="/ticket/new?o={{username_id}}" method="post">
                Subject:
                <input name="subject" type="text">
                <br/> Description:
                <input name="description" type="text">
                <button type="submit" value="create_ticket">Criar Ticket</button>
            </form>
        </div>


</body>

</html>
