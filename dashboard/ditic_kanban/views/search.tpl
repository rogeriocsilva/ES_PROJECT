% max_len = 80
<!DOCTYPE html>
<html>

<style>
    * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-weight: 300;
    text-decoration: none;
    color: rgba(0, 0, 0, 0.82);
}

hr {
    border: 0;
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    width: 900px;
}

body {
    background-color: rgb(242, 242, 242);
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    margin: 0;
}

body::-webkit-input-placeholder {
    /* WebKit browsers */
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body:-moz-placeholder {
    /* Mozilla Firefox 4 to 18 */
    font-family: 'Roboto', sans-serif;
    opacity: 1;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body::-moz-placeholder {
    /* Mozilla Firefox 19+ */
    font-family: 'Roboto', sans-serif;
    opacity: 1;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body:-ms-input-placeholder {
    /* Internet Explorer 10+ */
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

.wrapper {
    background-color: rgb(242, 242, 242);
    position: absolute;
    top: 50%;
    left: 0;
    width: 100%;
    height: 400px;
    margin-top: -200px;
    overflow: hidden;
}

.wrapper.form-success .container h1 {
    -webkit-transform: translateY(85px);
    -ms-transform: translateY(85px);
    transform: translateY(85px);
    background-color: rgb(242, 242, 242);
}

.container {
    max-width: 370px;
    margin: 0 auto;
    padding: 80px 0;
    height: 400px;
    text-align: center;
}

.container_t {
    max-width: 900px;
    margin: 0 auto;
    padding: 80px 0;
    height: 400px;
    text-align: center;
}

.container h1 {
    font-size: 40px;
    -webkit-transition-duration: 1s;
    transition-duration: 1s;
    -webkit-transition-timing-function: ease-in-put;
    transition-timing-function: ease-in-put;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}

.form_l input {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    background-color: rgb(255, 255, 255);
    width: 250px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l input:hover {
    background-color: rgba(255, 255, 255, 0.4);
}

.form_l input:focus {
    background-color: white;
    width: 300px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    font-family: 'Roboto';
}

.form_l button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    background-color: rgb(255, 255, 255);
    width: 100px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l button:hover {}

.form_t {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
    text-align: center;
}

.form_t input {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    background-color: rgb(255, 255, 255);
    width: 800px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: left;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_t input:hover {
    background-color: rgba(255, 255, 255, 0.4);
}

.form_t input:focus {
    background-color: white;
    width: 850px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    font-family: 'Roboto';
}

.form_t button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    background-color: rgb(255, 255, 255);
    width: 200px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_t button:hover {}

nav {
    display: inline-block;
    width: 100%;
    height: 60px;
    background-color: #3F51B5;
    padding-left: 40px;
    box-shadow: 2px 3px 10px #888888;
}

nav div {
    margin: 5px;
    float: left;
    height: 100%;
}

nav h1 {
    color: white;
    font-weight: 300;
    font-family: 'Roboto';
    font-size: 20pt;
    margin-top: 7px;
}

.account {
    float: right;
}

.user {
    float: right;
}

.tabela {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
    width: 900px;
    padding: 20px;
}

.tabela_out {
    padding: 20px 0;
    width: 900px;
    margin: 0 auto;
}

#performance {
    padding: 20px 0;
    margin-left: 70%;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}

#mean_time_to_resolve {
    padding: 20px 0;
    margin-left: 85%;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}
.search {
    float: right;
}

.search_box {
    font: 14px 'Open Sans', sans-serif;
    color: #fff;
    padding: 5px 5px 5px 5px;
    width: 200px;
    border: 0px solid #4DB6AC;
    border-radius: 2px;
    -moz-appearance: none;
    -webkit-appearance: none;
    box-shadow: none;
    outline: 0;
    margin: 0;
    background-color:rgb(93,102,186);
    margin-top: 10px;
    
}

.search_box:focus {
    width: 250px;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
}

::-webkit-input-placeholder {
   color: white;
}

:-moz-placeholder { /* Firefox 18- */
   color: white;  
}

::-moz-placeholder {  /* Firefox 19+ */
   color: white;  
}

:-ms-input-placeholder {  
   color: white;  
}
</style>

<head>
    <meta charset="UTF-8">
    <title>es — site</title>
    <link rel="stylesheet" href="/style.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,,200,300,500,700' rel='stylesheet' type='text/css'>

</head>
%rebase('skin')

<body>
    
<div class="tabela_out">
    <h1>{{email}}</h1>
    <h2># Tickets:<i>{{number_tickets}}</i></h2> 
    <table class="tabela">
    % for priority in sorted(tickets, reverse=True):
    <tr>
        <td valign="top">
            <h2>{{priority}}</h2>
        </td>
        <td valign="top">
            
           % for ticket in sorted(tickets[priority], reverse=True):
            <tr>
                    <td>
                        <a href="http://localhost/rt/Ticket/Display.html?id={{ticket['id']}}">
                            {{ticket['id']}}
                        </a>
                    </td>
                    <td>
                        <a href="http://localhost/rt/Ticket/Display.html?id={{ticket['id']}}">
                            {{ticket['status']}}
                        </a>
                    </td>
                    <td>
                        <a href="http://localhost/rt/Ticket/Display.html?id={{ticket['id']}}">
                            {{ticket['cf.{servico}']}}
                        </a>
                    </td>
                    <td>
                        <a href="http://localhost/rt/Ticket/Display.html?id={{ticket['id']}}">
                            {{ticket['requestors']}}
                        </a>
                    </td>
                    <td>
                        <a href="http://localhost/rt/Ticket/Display.html?id={{ticket['id']}}">
                            % subject = ticket['subject']
                            % if len(ticket['subject']) > max_len:
                            %   subject = ticket['subject'][:max_len]+'...'
                            % end
                            {{subject}}
                        </a>
                    </td>
                    <td>
                        <a href="http://localhost/rt/Ticket/Display.html?id={{ticket['id']}}">
                            Created: {{ticket['created']}}<br>
                            Last Update: {{ticket['lastupdated']}}
                        </a>
                    </td>
       </tr>
            % end
     
        </td>
    </tr>
    % end
</table>

<p>
<br>
    Time to execute: {{time_spent}}
</p>
    </div>

    </body>
</html>