<!DOCTYPE html>
<html lang="en">
<style>
	* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-weight: 300;
    text-decoration: none;
    color: rgba(0, 0, 0, 0.82);
}

hr {
    border: 0;
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    width: 900px;
}

body {
    background-color: rgb(242, 242, 242);
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    margin: 0;
}

body::-webkit-input-placeholder {
    /* WebKit browsers */
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body:-moz-placeholder {
    /* Mozilla Firefox 4 to 18 */
    font-family: 'Roboto', sans-serif;
    opacity: 1;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body::-moz-placeholder {
    /* Mozilla Firefox 19+ */
    font-family: 'Roboto', sans-serif;
    opacity: 1;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

body:-ms-input-placeholder {
    /* Internet Explorer 10+ */
    font-family: 'Roboto', sans-serif;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
}

.wrapper {
    background-color: rgb(242, 242, 242);
    position: absolute;
    top: 50%;
    left: 0;
    width: 100%;
    height: 400px;
    margin-top: -200px;
    overflow: hidden;
}

.wrapper.form-success .container h1 {
    -webkit-transform: translateY(85px);
    -ms-transform: translateY(85px);
    transform: translateY(85px);
    background-color: rgb(242, 242, 242);
}

.container {
    max-width: 370px;
    margin: 0 auto;
    padding: 80px 0;
    height: 400px;
    text-align: center;
}

.container_t {
    max-width: 900px;
    margin: 0 auto;
    padding: 80px 0;
    height: 400px;
    text-align: center;
}

.container h1 {
    font-size: 40px;
    -webkit-transition-duration: 1s;
    transition-duration: 1s;
    -webkit-transition-timing-function: ease-in-put;
    transition-timing-function: ease-in-put;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}

.form_l input {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    background-color: rgb(255, 255, 255);
    width: 250px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l input:hover {
    background-color: rgba(255, 255, 255, 0.4);
}

.form_l input:focus {
    background-color: white;
    width: 300px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    font-family: 'Roboto';
}

.form_l button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    background-color: rgb(255, 255, 255);
    width: 100px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_l button:hover {}

.form_t {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
    text-align: center;
}

.form_t input {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    border-bottom: 2px solid rgba(0, 0, 0, 0.12);
    background-color: rgb(255, 255, 255);
    width: 800px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: left;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_t input:hover {
    background-color: rgba(255, 255, 255, 0.4);
}

.form_t input:focus {
    background-color: white;
    width: 850px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    font-family: 'Roboto';
}

.form_t button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: 0;
    border: 0px solid rgb(0, 0, 0);
    background-color: rgb(255, 255, 255);
    width: 200px;
    border-radius: 0px;
    padding: 10px 15px;
    margin: 0 auto 10px auto;
    display: block;
    text-align: center;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.82);
    font-weight: 400;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
    font-weight: 300;
    font-family: 'Roboto';
}

.form_t button:hover {}

nav {
    display: inline-block;
    width: 100%;
    height: 60px;
    background-color: #3F51B5;
    padding-left: 40px;
    box-shadow: 2px 3px 10px #888888;
}

nav div {
    margin: 5px;
    float: left;
    height: 100%;
}

nav h1 {
    color: white;
    font-weight: 300;
    font-family: 'Roboto';
    font-size: 20pt;
    margin-top: 7px;
}

.account {
    float: right;
}

.user {
    float: right;
}

.tabela {
    background-color: white;
    padding: 20px 0;
    position: relative;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
    width: 900px;
    padding: 20px;
}

.tabela_out {
    padding: 20px 0;
    width: 900px;
    margin: 0 auto;
}

#performance {
    padding: 20px 0;
    margin-left: 70%;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}

#mean_time_to_resolve {
    padding: 20px 0;
    margin-left: 85%;
    z-index: 2;
    box-shadow: 2px 3px 10px #888888;
    border-radius: 3px;
    font-family: 'Roboto';
}
.search {
    float: right;
}

.search_box {
    font: 14px 'Open Sans', sans-serif;
    color: #fff;
    padding: 5px 5px 5px 5px;
    width: 200px;
    border: 0px solid #4DB6AC;
    border-radius: 2px;
    -moz-appearance: none;
    -webkit-appearance: none;
    box-shadow: none;
    outline: 0;
    margin: 0;
    background-color:rgb(93,102,186);
    margin-top: 10px;
    
}

.search_box:focus {
    width: 250px;
    -webkit-transition-duration: 0.25s;
    transition-duration: 0.25s;
}

::-webkit-input-placeholder {
   color: white;
}

:-moz-placeholder { /* Firefox 18- */
   color: white;  
}

::-moz-placeholder {  /* Firefox 19+ */
   color: white;  
}

:-ms-input-placeholder {  
   color: white;  
}
</style>
<head>

    % meta_refresh = get('meta_refresh', 0)
    {{!'<meta http-equiv="refresh" content="%s">' % meta_refresh if meta_refresh else ''}}
    <meta charset="UTF-8">
    <title>{{title}}</title>
    % graph_script = get('graph_script', '')
    % if graph_script:
    {{!graph_script}}
    % end

    
    <meta charset="UTF-8">
    <title>es — site</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    
</head>
<body >
    
    <nav>
     % username_id = get('username_id', '')
        <div class="imagem">
            <a href="/?o={{username_id}}"><img src="/static/img/home/ic_home_white_48dp_2x.png" alt="" style="max-width:50%"></a>
        </div>       


    % username = get('username', '')
        <div class="imagem">
        % if username!='':
        	<h1>{{username}}</h1>
        % end
        </div>

    	<div class="search">
            <form action="/search?o={{get('username_id', '')}}" method="post">
                <input class="search_box" name="search" type="search" placeholder="Search">
            </form>

        </div>

    </nav>
{{!base}}
</body>
</html>